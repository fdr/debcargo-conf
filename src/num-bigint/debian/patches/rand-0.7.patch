diff --git a/src/bigrand.rs b/src/bigrand.rs
index 4a13b29..d0ff2e0 100644
--- a/src/bigrand.rs
+++ b/src/bigrand.rs
@@ -1,6 +1,6 @@
 //! Randomization of big integers
 
-use rand::distributions::uniform::{SampleUniform, UniformSampler};
+use rand::distributions::uniform::{SampleUniform, UniformSampler, SampleBorrow};
 use rand::prelude::*;
 use rand::AsByteSliceMut;
 
@@ -124,16 +124,26 @@ impl UniformSampler for UniformBigUint {
     type X = BigUint;
 
     #[inline]
-    fn new(low: Self::X, high: Self::X) -> Self {
+    fn new<B1, B2>(low_b: B1, high_b: B2) -> Self
+        where B1: SampleBorrow<Self::X> + Sized,
+              B2: SampleBorrow<Self::X> + Sized
+    {
+        let low = low_b.borrow();
+        let high = high_b.borrow();
         assert!(low < high);
         UniformBigUint {
-            len: high - &low,
-            base: low,
+            len: high - low,
+            base: low.clone(),
         }
     }
 
     #[inline]
-    fn new_inclusive(low: Self::X, high: Self::X) -> Self {
+    fn new_inclusive<B1, B2>(low_b: B1, high_b: B2) -> Self
+        where B1: SampleBorrow<Self::X> + Sized,
+              B2: SampleBorrow<Self::X> + Sized
+    {
+        let low = low_b.borrow();
+        let high = high_b.borrow();
         assert!(low <= high);
         Self::new(low, high + 1u32)
     }
@@ -144,8 +154,11 @@ impl UniformSampler for UniformBigUint {
     }
 
     #[inline]
-    fn sample_single<R: Rng + ?Sized>(low: Self::X, high: Self::X, rng: &mut R) -> Self::X {
-        rng.gen_biguint_range(&low, &high)
+    fn sample_single<R: Rng + ?Sized, B1, B2>(low: B1, high: B2, rng: &mut R) -> Self::X
+        where B1: SampleBorrow<Self::X> + Sized,
+              B2: SampleBorrow<Self::X> + Sized
+    {
+        rng.gen_biguint_range(low.borrow(), high.borrow())
     }
 }
 
@@ -164,16 +177,26 @@ impl UniformSampler for UniformBigInt {
     type X = BigInt;
 
     #[inline]
-    fn new(low: Self::X, high: Self::X) -> Self {
+    fn new<B1, B2>(low_b: B1, high_b: B2) -> Self
+        where B1: SampleBorrow<Self::X> + Sized,
+              B2: SampleBorrow<Self::X> + Sized
+    {
+        let low = low_b.borrow();
+        let high = high_b.borrow();
         assert!(low < high);
         UniformBigInt {
-            len: into_magnitude(high - &low),
-            base: low,
+            len: into_magnitude(high - low),
+            base: low.clone(),
         }
     }
 
     #[inline]
-    fn new_inclusive(low: Self::X, high: Self::X) -> Self {
+    fn new_inclusive<B1, B2>(low_b: B1, high_b: B2) -> Self
+        where B1: SampleBorrow<Self::X> + Sized,
+              B2: SampleBorrow<Self::X> + Sized
+    {
+        let low = low_b.borrow();
+        let high = high_b.borrow();
         assert!(low <= high);
         Self::new(low, high + 1u32)
     }
@@ -184,8 +207,11 @@ impl UniformSampler for UniformBigInt {
     }
 
     #[inline]
-    fn sample_single<R: Rng + ?Sized>(low: Self::X, high: Self::X, rng: &mut R) -> Self::X {
-        rng.gen_bigint_range(&low, &high)
+    fn sample_single<R: Rng + ?Sized, B1, B2>(low: B1, high: B2, rng: &mut R) -> Self::X
+        where B1: SampleBorrow<Self::X> + Sized,
+              B2: SampleBorrow<Self::X> + Sized
+    {
+        rng.gen_bigint_range(low.borrow(), high.borrow())
     }
 }
 
diff --git a/tests/bigint.rs b/tests/bigint.rs
index 911bff0..59c8793 100644
--- a/tests/bigint.rs
+++ b/tests/bigint.rs
@@ -1093,7 +1093,7 @@ fn test_negative_shr() {
 fn test_random_shr() {
     use rand::distributions::Standard;
     use rand::Rng;
-    let mut rng = rand::thread_rng();
+    let rng = rand::thread_rng();
 
     for p in rng.sample_iter::<i64, _>(&Standard).take(1000) {
         let big = BigInt::from(p);
diff --git a/tests/rand.rs b/tests/rand.rs
index 666b764..fb84b1f 100644
--- a/tests/rand.rs
+++ b/tests/rand.rs
@@ -3,6 +3,9 @@
 extern crate num_bigint;
 extern crate num_traits;
 extern crate rand;
+extern crate rand_chacha;
+extern crate rand_isaac;
+extern crate rand_xorshift;
 
 mod biguint {
     use num_bigint::{BigUint, RandBigInt, RandomBits};
@@ -118,7 +121,7 @@ mod biguint {
             "57401636903146945411652549098818446911814352529449356393690984105383482703074355\
              67088360974672291353736011718191813678720755501317478656550386324355699624671",
         ];
-        use rand::prng::ChaChaRng;
+        use rand_chacha::ChaChaRng;
         seeded_value_stability::<ChaChaRng>(EXPECTED);
     }
 
@@ -137,7 +140,7 @@ mod biguint {
             "37805949268912387809989378008822038725134260145886913321084097194957861133272558\
              43458183365174899239251448892645546322463253898288141861183340823194379722556",
         ];
-        use rand::prng::IsaacRng;
+        use rand_isaac::IsaacRng;
         seeded_value_stability::<IsaacRng>(EXPECTED);
     }
 
@@ -156,7 +159,7 @@ mod biguint {
             "53041498719137109355568081064978196049094604705283682101683207799515709404788873\
              53417136457745727045473194367732849819278740266658219147356315674940229288531",
         ];
-        use rand::prng::XorShiftRng;
+        use rand_xorshift::XorShiftRng;
         seeded_value_stability::<XorShiftRng>(EXPECTED);
     }
 }
@@ -280,7 +283,7 @@ mod bigint {
             "501454570554170484799723603981439288209930393334472085317977614690773821680884844\
              8530978478667288338327570972869032358120588620346111979053742269317702532328",
         ];
-        use rand::prng::ChaChaRng;
+        use rand_chacha::ChaChaRng;
         seeded_value_stability::<ChaChaRng>(EXPECTED);
     }
 
@@ -299,7 +302,7 @@ mod bigint {
             "-14563174552421101848999036239003801073335703811160945137332228646111920972691151\
              88341090358094331641182310792892459091016794928947242043358702692294695845817",
         ];
-        use rand::prng::IsaacRng;
+        use rand_isaac::IsaacRng;
         seeded_value_stability::<IsaacRng>(EXPECTED);
     }
 
@@ -318,7 +321,7 @@ mod bigint {
             "49920038676141573457451407325930326489996232208489690499754573826911037849083623\
              24546142615325187412887314466195222441945661833644117700809693098722026764846",
         ];
-        use rand::prng::XorShiftRng;
+        use rand_xorshift::XorShiftRng;
         seeded_value_stability::<XorShiftRng>(EXPECTED);
     }
 }
diff --git a/tests/torture.rs b/tests/torture.rs
index 4f073d3..025fdeb 100644
--- a/tests/torture.rs
+++ b/tests/torture.rs
@@ -6,6 +6,7 @@ extern crate rand;
 
 use num_bigint::RandBigInt;
 use num_traits::Zero;
+use rand::rngs::SmallRng;
 use rand::prelude::*;
 
 fn test_mul_divide_torture_count(count: usize) {
